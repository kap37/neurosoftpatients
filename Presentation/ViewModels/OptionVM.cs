﻿using Presentation.Models;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Windows;

namespace Presentation.ViewModels
{
    /// <summary>
    /// The main Options viewmodel class. Responsible for creating new options.
    /// </summary>
    public class OptionVM : BindableBase
    {
        private bool? _dialogResult;

        public bool? DialogResult
        {
            get { return _dialogResult; }
            protected set
            {
                _dialogResult = value;
                RaisePropertyChanged("DialogResult");
            }
        }

        public DelegateCommand SaveOptionCommand { get; }

        public DelegateCommand CloseWindowCommand { get; }

        public Option Option { get; private set; }

        public ObservableCollection<string> Types { get; private set; }

        public string Name
        {
            get { return Option.Name; }
            set
            {
                Option.Name = value;
            }
        }

        public string Type
        {
            get { return Option.Type; }
            set
            {
                Option.Type = value;
            }
        }

        public OptionVM()
        {
            Types = new ObservableCollection<string>(Option.GetAvailableTypes());
            Option = new Option();

            SaveOptionCommand = new DelegateCommand(() =>
            {
                if (Name == null || Name.Length < 2)
                {
                    MessageBox.Show("You need to write at least 2 characters", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DialogResult = true;
            });

            CloseWindowCommand = new DelegateCommand(() =>
            {
                DialogResult = false;
            });
        }
    }
}

﻿using Presentation.Core.Provider;
using Presentation.Models;
using Presentation.Windows;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Presentation.ViewModels
{
    /// <summary>
    /// The main OptionsOrder viewmodel class. Contains all methods for managing options order.
    /// </summary>
    public class OptionsOrderVM : BindableBase
    {
        private Option selectedOption;
        private bool madeChanges;
        private readonly OptionsOrder optionsOrder = new OptionsOrder();
        private IOptionsProvider optionsProvider;
        private ValuesListVM valuesListVM;
        public Action DataGridFocus;
        public Action CloseAction { get; set; }
        public bool IsDatagridFocused { get; set; }
        public List<string> Types { get; }

        /// <summary>
        /// Contains the value indicating whether changes were made in the options order.
        /// </summary>
        public bool MadeChanges
        {
            get => madeChanges;
            private set
            {
                madeChanges = value;
                RaisePropertyChanged("MadeChanges");
                if (SaveChangesCommand != null)
                {
                    SaveChangesCommand.RaiseCanExecuteChanged();
                }
                if (CancelChangesCommand != null)
                {
                    CancelChangesCommand.RaiseCanExecuteChanged();
                }
            }
        }

        #region IsEnabled binding
        public bool UpEnabled
        {
            get
            {
                return SelectedOption != null && SelectedOption.Number != 0;
            }
        }

        public bool DownEnabled
        {
            get
            {
                return SelectedOption != null && SelectedOption.Number != optionsOrder.Options.Last().Number;
            }
        }

        public bool DeleteEnabled
        {
            get
            {
                return SelectedOption != null;
            }
        }
        #endregion

        public ObservableCollection<Option> Items
        {
            get { return optionsOrder.Options; }
        }

        public Option SelectedOption
        {
            get
            {
                return selectedOption;
            }
            set
            {
                selectedOption = value;
                RaisePropertyChanged("SelectedOption");
                RaiseCanExecuteCommands();
            }
        }

        public DataGrid Datagrid { get; set; }

        public DelegateCommand AddCommand { get; }
        public DelegateCommand RemoveCommand { get; }
        public DelegateCommand UpCommand { get; }
        public DelegateCommand DownCommand { get; }
        public DelegateCommand ShowValuesCommand { get; }
        public DelegateCommand SaveChangesCommand { get; }
        public DelegateCommand CancelChangesCommand { get; }
        public DelegateCommand SaveAndCloseCommand { get; }
        public DelegateCommand CancelAndCloseCommand { get; }

        public OptionsOrderVM()
        {
            Types = Option.GetAvailableTypes();
            optionsProvider = new JsonOptionsProvider();
            GetOptionsList();

            #region Initialize commands
            AddCommand = new DelegateCommand(() =>
            {
                var addOptionWindow = new AddOptionWindow();
                var result = addOptionWindow.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    var newOption = ((OptionVM)addOptionWindow.DataContext).Option;
                    optionsOrder.Add(newOption);
                    MadeChanges = true;
                    SelectedOption = Items.Last();
                    FocusToSelectedItem();
                }
            });

            RemoveCommand = new DelegateCommand(() =>
            {
                if (SelectedOption != null)
                {
                    if (MessageBox.Show($"Вы хотите удалить параметр \"{SelectedOption.Name}\"?", "Удаление", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK)
                    {
                        var number = SelectedOption.Number;
                        optionsOrder.Remove(SelectedOption.Number);
                        MadeChanges = true;
                        var newSelectedOption = Items.FirstOrDefault(o => o.Number == number);
                        if (newSelectedOption == null)
                        {
                            newSelectedOption = Items.FirstOrDefault(o => o.Number == number - 1);
                        }
                        if (newSelectedOption != null)
                        {
                            SelectedOption = newSelectedOption;
                            FocusToSelectedItem();
                        }
                    }
                }
            }, () => DeleteEnabled);

            UpCommand = new DelegateCommand(() =>
            {
                if (SelectedOption != null)
                    optionsOrder.Up(SelectedOption.Number);
                RaisePropertyChanged("Items");
                RaiseCanExecuteCommands();
                MadeChanges = true;
                FocusToSelectedItem();
            }, () => UpEnabled);

            DownCommand = new DelegateCommand(() =>
              {
                  if (SelectedOption != null)
                      optionsOrder.Down(SelectedOption.Number);
                  RaisePropertyChanged("Items");
                  RaiseCanExecuteCommands();
                  MadeChanges = true;
                  FocusToSelectedItem();
              }, () => DownEnabled);

            ShowValuesCommand = new DelegateCommand(() =>
            {
                valuesListVM = new ValuesListVM();
                valuesListVM.LoadValues(SelectedOption.Values);
                if (valuesListVM.Values.Count > 0)
                    valuesListVM.SelectedValue = valuesListVM.Values[0];
                var ValuesListWindow = new ValuesListWindow(valuesListVM);
                var result = ValuesListWindow.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    SelectedOption.Values = valuesListVM.Values.ToList();
                    MadeChanges = true;
                }
            });

            SaveChangesCommand = new DelegateCommand(() =>
            {
                optionsProvider.SaveOptions(optionsOrder.Options.ToList());
                MadeChanges = false;
                FocusToSelectedItem();
            }, () => MadeChanges);

            CancelChangesCommand = new DelegateCommand(() =>
            {
                GetOptionsList();
            }, () => MadeChanges);

            SaveAndCloseCommand = new DelegateCommand(() =>
            {
                if (MadeChanges)
                {
                    optionsProvider.SaveOptions(optionsOrder.Options.ToList());
                }
                CloseAction();
            });

            CancelAndCloseCommand = new DelegateCommand(() =>
            {
                CloseAction();
            });
            #endregion
        }

        private void RaiseCanExecuteCommands()
        {
            if (UpCommand != null)
            {
                UpCommand.RaiseCanExecuteChanged();
            }
            if (DownCommand != null)
            {
                DownCommand.RaiseCanExecuteChanged();
            }
            if (RemoveCommand != null)
            {
                RemoveCommand.RaiseCanExecuteChanged();
            }
        }

        private void FocusToSelectedItem()
        {
            if (Datagrid != null && SelectedOption != null)
            {
                Datagrid.UpdateLayout();
                var dgRow = (DataGridRow)Datagrid.ItemContainerGenerator.ContainerFromIndex(SelectedOption.Number);
                dgRow.MoveFocus(new System.Windows.Input.TraversalRequest(System.Windows.Input.FocusNavigationDirection.Next));
            }
        }

        /// <summary>
        /// Fills options order from provider.
        /// </summary>
        private void GetOptionsList()
        {
            optionsOrder.Options = new ObservableCollection<Option>(optionsProvider.GetOptions());
            RaisePropertyChanged("Items");
            foreach (var option in optionsOrder.Options)
            {
                option.OptionChanged += () =>
                {
                    MadeChanges = true;
                };
            }
            MadeChanges = false;
            IsDatagridFocused = true;
            SelectedOption = Items.FirstOrDefault();
            FocusToSelectedItem();
        }
    }
}

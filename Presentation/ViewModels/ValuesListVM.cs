﻿using Presentation.Util;
using Presentation.Windows;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace Presentation.ViewModels
{
    /// <summary>
    /// Contains methods and commands for managing a list of values ​​for the option.
    /// </summary>
    public class ValuesListVM : BindableBase
    {
        private string selectedValue;
        private bool? _dialogResult;
        private int selectedIndex;
        private AddValueWindow addValueWindow;

        public ValuesListVM()
        {
            AddCommand = new DelegateCommand(() =>
            {
                AddedValue = null;
                addValueWindow = new AddValueWindow(this);
                var result = addValueWindow.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    Values.Add(AddedValue);
                }
            });

            EditCommand = new DelegateCommand(() =>
            {
                AddedValue = SelectedValue;
                addValueWindow = new AddValueWindow(this);
                var result = addValueWindow.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    Values[SelectedIndex] = AddedValue;
                }
            }, () => EditEnabled);

            DeleteCommand = new DelegateCommand(() =>
            {
                if (SelectedValue != null)
                {
                    if (MessageBox.Show($"Вы хотите удалить \"{SelectedValue}\" из списка?", "Удаление", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK)
                    {
                        Values.RemoveAt(SelectedIndex);
                    }
                }
            }, () => DeleteEnabled);

            UpCommand = new DelegateCommand(() =>
            {
                if (SelectedValue != null)
                {
                    var newIndex = SelectedIndex - 1;
                    Swap(SelectedIndex, newIndex);
                    SelectedIndex = newIndex;
                }
            }, () => UpEnabled);

            DownCommand = new DelegateCommand(() =>
            {
                if (SelectedValue != null)
                {
                    var newIndex = SelectedIndex + 1;
                    Swap(SelectedIndex, newIndex);
                    SelectedIndex = newIndex;
                }
            }, () => DownEnabled);

            SaveValuesCommand = new DelegateCommand(() =>
            {
                DialogResult = true;
            });

            CloseWindowCommand = new DelegateCommand(() =>
            {
                DialogResult = false;
            });

            SaveNewValueCommand = new DelegateCommand(() =>
            {
                if (AddedValue == null || AddedValue.Length < 1)
                {
                    MessageBox.Show("You need to write at least 1 characters", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DialogCloser.SetDialogResult(addValueWindow, true);

            });

            CloseAddValueWindowCommand = new DelegateCommand(() =>
            {
                DialogCloser.SetDialogResult(addValueWindow, false);
            });
        }

        public string AddedValue { get; set; }
        public ObservableCollection<string> Values { get; set; }
        public DelegateCommand AddCommand { get; }
        public DelegateCommand DeleteCommand { get; }
        public DelegateCommand UpCommand { get; }
        public DelegateCommand DownCommand { get; }
        public DelegateCommand EditCommand { get; }
        public DelegateCommand SaveValuesCommand { get; }
        public DelegateCommand CloseWindowCommand { get; }
        public DelegateCommand SaveNewValueCommand { get; }
        public DelegateCommand CloseAddValueWindowCommand { get; }
        public bool DeleteEnabled => SelectedValue != null;
        public bool EditEnabled => DeleteEnabled;

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
                RaisePropertyChanged("SelectedIndex");
            }
        }

        public bool? DialogResult
        {
            get { return _dialogResult; }
            protected set
            {
                _dialogResult = value;
                RaisePropertyChanged("DialogResult");
            }
        }

        public string SelectedValue
        {
            get
            {
                return selectedValue;
            }
            set
            {
                selectedValue = value;
                if (UpCommand != null)
                {
                    UpCommand.RaiseCanExecuteChanged();
                }
                if (DownCommand != null)
                {
                    DownCommand.RaiseCanExecuteChanged();
                }
                if (DeleteCommand != null)
                {
                    DeleteCommand.RaiseCanExecuteChanged();
                }
                if (EditCommand != null)
                {
                    EditCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public bool UpEnabled
        {
            get
            {
                return SelectedValue != null && Values.IndexOf(SelectedValue) != 0;
            }
        }

        public bool DownEnabled
        {
            get
            {
                return SelectedValue != null && Values.IndexOf(SelectedValue) != Values.Count - 1;
            }
        }

        public void LoadValues(List<string> values)
        {
            if (values == null)
            {
                values = new List<string>();
            }
            Values = new ObservableCollection<string>(values);
        }

        private void Swap(int i, int j)
        {
            if (i >= 0 && j >= 0)
            {
                var element1 = Values[i];
                var element2 = Values[j];
                Values[i] = element2;
                Values[j] = element1;
            }
        }
    }
}

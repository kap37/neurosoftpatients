﻿using Presentation.ViewModels;
using System.Windows;

namespace Presentation.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddValueWindow.xaml
    /// </summary>
    public partial class AddValueWindow : Window
    {
        public AddValueWindow(ValuesListVM valuesListVM)
        {
            InitializeComponent();
            DataContext = valuesListVM;
            valueTb.Focus();
        }
    }
}

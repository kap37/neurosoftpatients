﻿using Presentation.ViewModels;
using System.Windows;

namespace Presentation.Windows
{
    /// <summary>
    /// Логика взаимодействия для ValuesListWindow.xaml
    /// </summary>
    public partial class ValuesListWindow : Window
    {
        public ValuesListWindow(ValuesListVM valuesListVM)
        {
            InitializeComponent();
            DataContext = valuesListVM;
        }
    }
}

﻿using System;
using System.Windows;

namespace Presentation.Windows
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class OptionsWindow : Window
    {
        public OptionsWindow()
        {
            InitializeComponent();
            vm.CloseAction = new Action(Close);
            vm.Datagrid = datagrid;
        }
    }
}

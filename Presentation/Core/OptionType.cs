﻿namespace Presentation.Core
{
    /// <summary>
    /// Represents an option type.
    /// </summary>
    public enum OptionType
    {
        SimpleLine,
        HistoryLine,
        ValueFromList,
        SetOfValuesFromList
    }
}

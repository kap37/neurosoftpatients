﻿using Presentation.Models;
using System.Collections.Generic;

namespace Presentation.Core.Provider
{
    /// <summary>
    /// Provides an interface for manage options providers.
    /// </summary>
    public interface IOptionsProvider
    {
        /// <summary>
        /// Gets a list of Options from some source.
        /// </summary>
        /// <returns></returns>
        List<Option> GetOptions();

        /// <summary>
        /// Saves a list of Options to some data set.
        /// </summary>
        /// <param name="options">The list that contains information about options.</param>
        void SaveOptions(List<Option> options);
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Presentation.Models;
using Presentation.Util;

namespace Presentation.Core.Provider
{
    public class JsonOptionsProvider : IOptionsProvider
    {
        /// <summary>
        /// Path to the source file.
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Provides methods for interacting with the Options source as a JSON-file.
        /// </summary>
        public JsonOptionsProvider()
        {
            FilePath = Configuration.JsonPath;
        }

        /// <summary>
        /// Gets a list of Options from JSON-file.
        /// </summary>
        /// <returns></returns>
        public List<Option> GetOptions()
        {
            List<Option> options = new List<Option>();
            if (File.Exists(FilePath))
            {
                var json = File.ReadAllText(FilePath);
                options = JsonConvert.DeserializeObject<List<Option>>(json);
            }
            return options;
        }

        /// <summary>
        /// Saves a list of Options to JSON-file.
        /// </summary>
        /// <param name="options">The list that contains information about options.</param>
        public void SaveOptions(List<Option> options)
        {
            if (options != null)
            {
                var json = JsonConvert.SerializeObject(options);
                File.WriteAllText(FilePath, json);
            }
            else
            {
                throw new ArgumentNullException(nameof(options));
            }
        }
    }
}

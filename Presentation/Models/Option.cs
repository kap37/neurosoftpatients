﻿using Presentation.Core;
using Presentation.Util;
using Prism.Mvvm;
using System;
using System.Collections.Generic;

namespace Presentation.Models
{
    /// <summary>
    /// Provides methods for working with options.
    /// </summary>
    public class Option : BindableBase
    {
        private string name;
        private OptionType type;

        /// <summary>
        /// Occurs when a option properties values changes.
        /// </summary>
        public event Action OptionChanged;

        public Option() => type = OptionType.SimpleLine;

        /// <summary>
        /// Contains option name.
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OptionChanged?.Invoke();
            }
        }

        /// <summary>
        /// Represents option number in order.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Contains available option values.
        /// </summary>
        public List<string> Values { get; set; }

        /// <summary>
        /// Represents option type: SimpleLine, HistoryLine, ValueFromList or SetOfValuesFromList.
        /// </summary>
        public string Type
        {
            get
            {
                switch (type)
                {
                    case OptionType.SimpleLine:
                        return StringConstants.SimpleLine;
                    case OptionType.HistoryLine:
                        return StringConstants.HistoryLine;
                    case OptionType.ValueFromList:
                        return StringConstants.ValueFromList;
                    case OptionType.SetOfValuesFromList:
                        return StringConstants.SetOfValuesFromList;
                    default:
                        throw new Exception($"Unknown option type: {type.ToString()}");
                };
            }
            set
            {
                switch (value)
                {
                    case StringConstants.SimpleLine:
                        type = OptionType.SimpleLine;
                        break;
                    case StringConstants.HistoryLine:
                        type = OptionType.HistoryLine;
                        break;
                    case StringConstants.ValueFromList:
                        type = OptionType.ValueFromList;
                        break;
                    case StringConstants.SetOfValuesFromList:
                        type = OptionType.SetOfValuesFromList;
                        break;
                    default:
                        throw new Exception($"Unknown option type: {value}");
                };
                OptionChanged?.Invoke();
                RaisePropertyChanged("IsMultiOption");
            }
        }

        /// <summary>
        /// Returns 'true' when option type is ValueFromList or SetOfValuesFromList, returns 'false' when type is SimpleLine, HistoryLine.
        /// </summary>
        public bool IsMultiOption
        {
            get
            {
                return type == OptionType.ValueFromList || type == OptionType.SetOfValuesFromList;
            }
        }

        /// <summary>
        /// Returns list of available option types.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAvailableTypes()
        {
            return new List<string>() { StringConstants.SimpleLine, StringConstants.HistoryLine, StringConstants.ValueFromList, StringConstants.SetOfValuesFromList };
        }
    }
}
﻿using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Linq;

namespace Presentation.Models
{
    /// <summary>
    /// Provides methods for adding new options, upping, downing and removing options from the options order.
    /// </summary>
    public class OptionsOrder : BindableBase
    {
        /// <summary>
        /// Contains collection of options in order.
        /// </summary>
        public ObservableCollection<Option> Options;

        /// <summary>
        /// Adds new option to order.
        /// </summary>
        /// <param name="option">Option to add.</param>
        public void Add(Option option)
        {
            option.Number = Options.Count > 0 ? Options.Max(o => o.Number) + 1 : 0;
            Options.Add(option);
        }

        /// <summary>
        /// Removes option from order by option number.
        /// </summary>
        /// <param name="index">Removable Option number.</param>
        public void Remove(int index)
        {
            if (index >= 0 && index < Options.Count)
            {
                Options.RemoveAt(index);
                Options.Where(o => o.Number > index).ToList().ForEach(o => o.Number--);
            }
        }

        /// <summary>
        /// Raises the option in order by one position.
        /// </summary>
        /// <param name="index">Raisable Option number.</param>
        public void Up(int index)
        {
            if (index > 0 && index < Options.Count)
            {
                var upped = Options.FirstOrDefault(o => o.Number == index);
                var downed = Options.FirstOrDefault(o => o.Number == index - 1);
                if (upped != null && downed != null)
                {
                    upped.Number--;
                    downed.Number++;
                    Options = new ObservableCollection<Option>(Options.OrderBy(o => o.Number));
                }
            }
        }

        /// <summary>
        /// Lowers the option in order by one position.
        /// </summary>
        /// <param name="index">Lowerable Option number.</param>
        public void Down(int index)
        {
            Up(index + 1);
        }
    }
}

﻿using System.Windows;

namespace Presentation.Util
{
    /// <summary>
    /// Responsible for managing elements focus.
    /// </summary>
    public static class FocusExtension
    {
        /// <summary>
        /// Gets 'IsFocused' property for control.
        /// </summary>
        /// <param name="obj">Needed control.</param>
        /// <returns>'IsFocused' property value.</returns>
        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }

        /// <summary>
        /// Sets 'IsFocused' property for control.
        /// </summary>
        /// <param name="obj">Needed control.</param>
        /// <param name="value">New property value.</param>
        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
        }


        /// <summary>
        /// Dependency property 'IsFocused' of target control.
        /// </summary>
        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached(
                "IsFocused", typeof(bool), typeof(FocusExtension),
                new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        private static void OnIsFocusedPropertyChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus();
            }
        }
    }

}

﻿using System.Configuration;

namespace Presentation.Util
{
    /// <summary>
    /// Contains application configuration properties.
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Path to JSON-file.
        /// </summary>
        public static string JsonPath => ConfigurationManager.AppSettings["json-path"] ?? string.Empty;
    }
}

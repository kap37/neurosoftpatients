﻿namespace Presentation.Util
{
    /// <summary>
    /// Contains string constants of application.
    /// </summary>
    public static class StringConstants
    {
        public const string SimpleLine = "Простая строка";
        public const string HistoryLine = "Строка с историей";
        public const string ValueFromList = "Значение из списка";
        public const string SetOfValuesFromList = "Набор значений из списка";
    }
}

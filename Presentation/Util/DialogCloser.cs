﻿using System.Windows;

namespace Presentation.Util
{
    /// <summary>
    /// Responsible for the dialog result of windows.
    /// </summary>
    public static class DialogCloser
    {
        /// <summary>
        /// Dependency property of target window.
        /// </summary>
        public static readonly DependencyProperty DialogResultProperty =
            DependencyProperty.RegisterAttached(
                "DialogResult",
                typeof(bool?),
                typeof(DialogCloser),
                new PropertyMetadata(DialogResultChanged));

        private static void DialogResultChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            var window = d as Window;
            if (window != null)
            {
                try
                {
                    window.DialogResult = e.NewValue as bool?;
                    if (window.DialogResult != null)
                        window.Close();
                }
                catch { }
            }
        }

        /// <summary>
        /// Sets dialog result for window.
        /// </summary>
        /// <param name="target">Target window.</param>
        /// <param name="value">Dialog result value.</param>
        public static void SetDialogResult(Window target, bool? value)
        {
            target.SetValue(DialogResultProperty, value);
        }
    }
}
